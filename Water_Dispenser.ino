//http://embed.plnkr.co/3VUsekP3jC5xwSIQDVHx/preview
#include <assert.h>
#include <HX711.h>


#define COL_1 A5
#define COL_2 11
#define COL_3 12
#define COL_4 13
#define COL_5 A0
#define COL_6 A1
#define COL_7 A2
#define COL_8 A3


#define ROW_1 2
#define ROW_2 3
#define ROW_3 4
#define ROW_4 5
#define ROW_5 6
#define ROW_6 7

#define motor A4

#define DOUT 8
#define CLK 9

#define taster 10

#define calibration_factor 1

HX711 scale(DOUT, CLK);

long amount = 0; 

const long total = 6000; //schauen, was das entspricht

const long fullGlass = 25000; // Gewicht volles Glas (150 Sekunden Füllen)

const long emptyGlass = 18700; // Gewicht leeres Glas

const byte cols[] = {
    COL_1, COL_2, COL_3, COL_4, COL_5, COL_6, COL_7, COL_8
};

const byte rows[] = {
    ROW_1, ROW_2, ROW_3, ROW_4, ROW_5, ROW_6, B00000000, B00000000
};


boolean isPushed(){
    if (abs(scale.get_units()) > 6000) {
        return true;
    }
    return false;
}

boolean wasPushed(){
    unsigned char tast = !digitalRead(taster);
    if (tast == 1){
        return true;
    } else {
        return false;
    }
}

void blinkRows(int r){ 
    assert(r >= 0); // r from zero to 8 rows (1 Freiheit)
    if (r > 8) {
        r = 8;
    }
    for (int i = 0; i < r; i++){
      digitalWrite(cols[i], HIGH);
    }  
}

void blinkOn(int side){
  assert(side == 1 || side == 0);
  if (side == 1){
    for (int i = 0; i < 4; i++){
      digitalWrite(cols[i], HIGH);
      delay(50);
    } 
  } else if (side == 0) {
    for (int i = 7; i > 3; i--){
      digitalWrite(cols[i], HIGH);
      delay(50);
    } 
  }
  
}

void blinkOff(bool side){
  if (side){
    for (int i = 0; i < 4; i++){
      digitalWrite(cols[i], LOW);
      delay(200);
    } 
  } else {
    for (int i = 7; i > 3; i--){
      digitalWrite(cols[i], LOW);
      delay(200);
    } 
  }
  
}

void blinkAll(int speedBlink){
    assert(speedBlink > 0);
    for (int i = 0; i < 8; i++){
      digitalWrite(cols[i], HIGH);
      delay(speedBlink);
      digitalWrite(cols[i], LOW);
      delay(speedBlink);
    }  
}

void displayDrinking(){
    displayAll(1000);
    delay(2000);
    for (int i = 7; i >= 0; i--){
      digitalWrite(cols[i], LOW);
      delay(2000);
    }  
}

void displayAll(int speedBlink){
    assert(speedBlink > 0);
    for (int i = 0; i < 8; i++){
      digitalWrite(cols[i], HIGH);
      delay(1);
    }
}

void displayAllOff(int speedBlink){
    assert(speedBlink > 0);
    for (int i = 0; i < 8; i++){
      digitalWrite(cols[i], LOW);
      delay(speedBlink);
    }
}


void setup() {
  
    // Open serial port
    Serial.begin(9600);

     //Taster
    pinMode(taster, INPUT_PULLUP);

    //scale
    scale.set_scale(calibration_factor);
    scale.tare();
    
    // Set all used pins to OUTPUT
    // This is very important! If the pins are set to input
    // the display will be very dim.
    for (byte i = 2; i <= 13; i++){
      if(i != 8 && i != 9){
        pinMode(i, OUTPUT);
      }
    }   
    pinMode(A0, OUTPUT);
    pinMode(A1, OUTPUT);
    pinMode(A2, OUTPUT);
    pinMode(A3, OUTPUT);
    pinMode(A4, OUTPUT);
    pinMode(A5, OUTPUT);
    randomSeed(analogRead(4));

}

void loop() {
    Serial.println("Push to start");
    displayAll(10);
    while(!wasPushed()){
      
    }
    displayAllOff(50);
    if (amount >= total){
        Serial.println("Genug getrunken!");
        Serial.println("Push to start new dispensing");
        while(!wasPushed()){
            blinkAll(50); // genug getrunken
        }
        amount = 0;
        Serial.println("Restarted");
    } else {
        while(scale.get_units() < fullGlass){
            digitalWrite(motor, HIGH);
        }
        delay(1000);
        long afterFill = scale.get_units() - emptyGlass;
        Serial.println("afterFill:");
        Serial.println(afterFill);
        digitalWrite(motor, LOW);
        Serial.println("Time to drink");
        Serial.println("Push to weight glass");
        while(!wasPushed()){
            blinkAll(30); 
        }
        long afterDrink = scale.get_units() - emptyGlass;
        Serial.println("afterDrink:");
        Serial.println(afterDrink);
        amount += afterFill - afterDrink;
        Serial.println("amount:");
        Serial.println(amount);
        double mtx = (amount * 8)/total;
        Serial.println("Matrix:");        
        Serial.println(mtx);
        blinkRows((int) mtx);
        delay(10000);
        displayAllOff(500);
    }
    
}

