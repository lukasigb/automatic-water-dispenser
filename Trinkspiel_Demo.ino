//http://embed.plnkr.co/3VUsekP3jC5xwSIQDVHx/preview
#include <assert.h>
#include <HX711.h>


#define COL_1 A5
#define COL_2 11
#define COL_3 12
#define COL_4 13
#define COL_5 A0
#define COL_6 A1
#define COL_7 A2
#define COL_8 A3


#define ROW_1 2
#define ROW_2 3
#define ROW_3 4
#define ROW_4 5
#define ROW_5 6
#define ROW_6 7

#define motor A4

#define DOUT 8
#define CLK 9

#define button 10

#define calibration_factor 1

HX711 scale(DOUT, CLK);


const int glassSize = 150;

const int steps = 4;

const byte cols[] = {
    COL_1, COL_2, COL_3, COL_4, COL_5, COL_6, COL_7, COL_8
};

const byte rows[] = {
    ROW_1, ROW_2, ROW_3, ROW_4, ROW_5, ROW_6, B00000000, B00000000
};



void setup() {
    // Open serial port
    Serial.begin(9600);

    //Taster
    pinMode(button, INPUT_PULLUP);

    //scale
    scale.set_scale(calibration_factor);
    scale.tare();
    
    // Set all used pins to OUTPUT
    // This is very important! If the pins are set to input
    // the display will be very dim.
    for (byte i = 2; i <= 13; i++){
      if(i != 8 && i != 9){
        pinMode(i, OUTPUT);
      }
    }   
    pinMode(A0, OUTPUT);
    pinMode(A1, OUTPUT);
    pinMode(A2, OUTPUT);
    pinMode(A3, OUTPUT);
    pinMode(A4, OUTPUT);
    pinMode(A5, OUTPUT);
    randomSeed(analogRead(4));

    
    
}

void loop() {
   Serial.println("Who is drinking? ...");
   Serial.println("Place your finger on the taster! ...");
   displayAll();
   while(!wasPushed()){} //testet, ob Taster gedrückt ist
   displayAllOff();
   for (int i = 0 ; i < 5 ; i++){
      blinkAll(50);
   }
   int scl = scale.get_units();
   int rndSide = abs(scl)%2;
   // Serial.println(scl);
   blinkOn(rndSide); 
   delay(2000);
   int rnds[steps];
   for (int i = 0; i < steps; i++){
      scl = scale.get_units();
      rnds[i] = abs(scl)%2;
   }
   int rndFill;
   for (int i = 0; i < steps; i++){
      rndFill += rnds[i];
   }
   rndFill *= (glassSize/(steps - 1));
   // Serial.println(rndFill);
   digitalWrite(motor, HIGH);
   for (int i = 0; i < rndFill; i++){
      delay(1000);
   }
   digitalWrite(motor, LOW);
   blinkOff(rndSide);   
   Serial.println("Start drinking! ...");
   displayDrinking();
   delay(2000);
   Serial.println("Neue Rundeeeee ...");
   delay(1000);
}

void blinkOn(int side){
  assert(side == 1 || side == 0);
  if (side == 1){
    for (int i = 0; i < 4; i++){
      digitalWrite(cols[i], HIGH);
      delay(50);
    } 
  } else if (side == 0) {
    for (int i = 7; i > 3; i--){
      digitalWrite(cols[i], HIGH);
      delay(50);
    } 
  }
  
}

void blinkOff(bool side){
  if (side){
    for (int i = 0; i < 4; i++){
      digitalWrite(cols[i], LOW);
      delay(200);
    } 
  } else {
    for (int i = 7; i > 3; i--){
      digitalWrite(cols[i], LOW);
      delay(200);
    } 
  }
  
}

void blinkAll(int speedBlink){
    assert(speedBlink > 0);
    for (int i = 0; i < 8; i++){
      digitalWrite(cols[i], HIGH);
      delay(speedBlink);
      digitalWrite(cols[i], LOW);
      delay(speedBlink);
    }  
}

void displayDrinking(){
    displayAll();
    delay(2000);
    for (int i = 7; i >= 0; i--){
      digitalWrite(cols[i], LOW);
      delay(2000);
    }  
}

void displayAll(){
    for (int i = 0; i < 8; i++){
      digitalWrite(cols[i], HIGH);
      delay(1);
    }
}

void displayAllOff(){
    for (int i = 0; i < 8; i++){
      digitalWrite(cols[i], LOW);
      delay(1);
    }
}

boolean wasPushed(){
    unsigned char tast = !digitalRead(button);
    if (tast == 1){
        return true;
    } else {
        return false;
    }
}


